import sys
from HTMLParser import HTMLParser

class LinkExtractor(HTMLParser):
	def __init__(self, output_list=None):
		HTMLParser.__init__(self)
		if output_list is None:
			self.output_list = []
		else:
			self.output_list = output_list
	
	def handle_starttag(self, tag, attrs):
		if tag == 'a':
			self.output_list.append(dict(attrs).get('href'))

p = LinkExtractor()
with open(sys.argv[1]) as file:
	thing = file.read()
p.feed(thing)
for line in p.output_list:
	print(line)

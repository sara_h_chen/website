# Medical Computer Risks?
---------------------------------------------
Volume 5 Issue 15

*Prentiss Riddle* - __22 July 1987__

This is a request for information on examples of problems caused by computer errors in the medical field.  
It will hopefully provide a significant improvement in the speed, detail and reliability of patient charting.

[Medical Computer Risks? 22 July 1987, Volume 5 Issue 15](https://catless.ncl.ac.uk/Risks/5.15.html#subj9)

# Microchipping Children Like Pets?
---------------------------------------------
Volume 23 Issue 71

*Peter H. Coffin* - __11 February 2005__

### RFID Tagging Elementary School Children
The only grade school in Sutter, California, is requiring their young students to wear radio frequency identification (RFID) badges that track their every move. Some parents are outraged; but who can blame them, with their children being tracked by the same radio frequency and scanner technology that companies use to track livestock and product inventory?

While similar devices are being tested at several schools in Japan to alert parents when their children arrive and leave, Brittan appears to be the first U.S. school district to embrace such a monitoring system.

It seems obvious that children will eventually find a way to circumvent this system; but with increasingly extreme measures being taken to track individuals, will we someday end up tagging people at birth, by implanting RFID chips under the skin?

[RFID Tagging Elementary School Children; 11 February 2005, Volume 23 Issue 71](https://catless.ncl.ac.uk/Risks/23.71.html#subj7)

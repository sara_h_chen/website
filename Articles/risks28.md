# The Dangers of Computers in Hospitals
---------------------------------------------
Volume 23 Issue 87

*Ken Knowlton* - __29 April 2005__
### Are computers a threat to the well-being of hospital patients?
Computers are as much a part of a hospital's working environment as any office. They keep track of patients records and allow for fast communication between experts from across the country. It would then be unreasonable to remove computers from hospitals but what if they were harmful towards patients?

Computer keyboards, as most computer users know, collect vast amounts of dust and dead skin which lead to large colonies of bacteria building up within the keyboard. Any member of medical staff that touches a keyboard will be exposing themselves to a large amount of bacteria and with the rise of antibiotic resistant bacteria within hospitals this becomes very dangerous.

Unfortunately not much can be done for the poor, disease-ridden keyboards. Cleaning them with water has no effect and even though disinfectant would kill the bacteria, it also damages the computer. Although nothing can be done for the keyboards, it is up to medical staff to ensure they maintain a high level of hygiene and are aware that they may even have to wash their hands after sending an email.

[The Downside of Wired Hospitals; 29 April 2005, Volume 23 Issue 87](https://catless.ncl.ac.uk/Risks/23.87.html#subj2.1)

# Drug-Error Risk At Hospitals Tied to Computers
---------------------------------------------
Volume 23 Issue 78

*Monty Solomon* - __10 March 2005__

Hospital computer systems widely touted as the best way to eliminate dangerous medication mix-ups can actually introduce many errors. It was found that some patients were put at risk of getting double doses of their medicine while other get none at all.  

22 types of mistakes were identified, such as failing to stop old medications when adding new ones or forgetting that the computer automatically suspended medications after surgery.  

Medicine estimates that errors of all kinds kill 44,000 to 98,000 patients a year.

[Drug-error risk at hospitals tied to computers; 10 March 2005, Volume 23 Issue 78](https://catless.ncl.ac.uk/Risks/23.78.html#subj2)

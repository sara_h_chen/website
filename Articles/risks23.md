# Crashing Stock Exchange Could Have Been Saved by Back-ups
---------------------------------------------
Volume 1 Issue 18

*Anonymous* - __30 September 1985__

### How important are back-ups, really?
We are all constantly warned to 'keep back-ups' and 'save regularly' which some people do and it is generally considered very good practice. Why then, did the American Stock Exchange not do the same?

When hurricane Gloria approached New York, the New York and American Stock Exchanges did not open. The hurricane had no effect on the opening of the Midwest Exchange, or so they thought. 40 minutes after opening the exchange was forced to close as the nationwide computer system failed. The Stock Exchange literally crashed. The central computer was of course located in New York and with no back-up systems in place the entire system went down. The Director of the Exchange was quoted saying,
> "Well, this has got to change"

So remember always keep back-ups.

[Lack of a backup computer closes stock exchange; 30 September 1985, Volume 1, Issue 18](https://catless.ncl.ac.uk/Risks/1.18.html#subj1)

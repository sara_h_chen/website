# Artificial Intelligence and the Internet of Things
---------------------------------------------
Volume 28 Issue 54

*Hendricks Dewayne* - __22 February 2015__

### Fears that AI are becoming 'too smart'
Fearing the power of AI is not a new thing. It is the basis for many films and has an inexplicable way to scare and excite. But is there actually anything to fear?

Stephen Hawking, Bill Gates and Elon Musk all seem to think so. They fear that full development of artificial intelligence could spell the end of the human race and with phrases like; "It's our biggest existential threat" and "summoning the demon" you can't help but be a little worried.

There is nothing to fear for now, however, as AI's are nowhere near the point we can describe as 'fully developed'. Deep Blue and Alpha Go are fantastic advancements in AI but they are unlikely to take over the world.

The fear comes from the idea of machines capable of reprogramming themselves, without the need for human input, to overcome obstacles. What if humans themselves become considered 'an obstacle'.

A much brighter view of the future comes from Kevin Kelly who believes AI will bring everything in the home to life and says:

>"Everything that we formerly electrified, we will now cognitize".

A future I am much more willing to be a part of.

[What will happen when the Internet of things becomes artificially intelligent?; 22 February 2015, Volume 28 Issue 54](https://catless.ncl.ac.uk/Risks/28.54.html#subj2)

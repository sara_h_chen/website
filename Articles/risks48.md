# Bank Employees Potentially the Biggest Risk to Your Account
---------------------------------------------
Volume 23 Issue 84

*Caskey L. Dickson* - __7 April 2005__

### Bank Agent Gives Out Personal Information
A lady found a credit card dropped in the parking lot of a mall, and decided to call the 1800 number on the back of the card to find out where she should return it. After getting in touch with an agent and telling the agent the story of the found card, she gave the name on the card along with the account number. Completely unbidden, the agent proceeds to tell her three things:

  - The card has not yet been reported stolen
  - The cardholder's billing address
  - The cardholder's home phone number

When the agent was told that she was essentially enabling identity theft, her reply was shocking:
>"__Oh, that's not a problem.__"

Fortunately, the good Samaritan dropped the card off at the bank branch she passed en route home. It is truly more than a little disturbing that the bank's training program did not condition agents to resist giving out personal information so easily.

[BofA Agent Gives Out Personal Information to Finder of Lost VISA Card; 7 April 2005, Volume 23 Issue 84](https://catless.ncl.ac.uk/Risks/23.84.html#subj5)

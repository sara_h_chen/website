# April Fool's Prank Goes Wrong
---------------------------------------------
Volume 23 Issue 07

*Lillie Coney* - __4 December 2003__

### Prank E-mail Frees Detained Kidnapper
A Homeland Department employee's prank e-mail prompted the release of an immigration agency detainee who had been convicted of kidnapping, according to the department's Inspector General. Thankfully, the unidentified detainee turned himself in two days later.

16 ICE detention officers and supervisors received an e-mail advising them that the detainee's citizenship had been established with a Puerto Rican birth certificate, authorizing his release.

>At the end of the first e-mail, the employee wrote, "Now about that bridge I'm selling. April Fools!" Nine minutes later, a second e-mail was sent, saying, "In case you didn't get to the end of my previous message, here's what really happened today." The second e-mail contained the actual orders, that the detainee was to be deported to the Dominican Republic.

Another homeland officer who read the first prank e-mail but did not note the April Fools reference, and did not read the second e-mail, processed the paperwork that authorized the detainee's release.

[April Fool's E-mail Freed Detained Kidnapper; 4 December 2003, Volume 23 Issue 07](https://catless.ncl.ac.uk/Risks/23.07.html#subj11)

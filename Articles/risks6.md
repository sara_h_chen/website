# California Halts E-Vote Certification
---------------------------------------------
Volume 23 Issue 01

*Monty Solomon* - __4 November 2003__

Uncertified software may have been installed on electronic voting machines used in one California county.

Carrel said his office had recently received "disconcerting information", hence the certification of the new voting machines were halted.

[California Halts E-Vote Certification; 4 November 2003, Volume 23 Issue 01](https://catless.ncl.ac.uk/Risks/23.01.html#subj4)

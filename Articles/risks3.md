# Is There A Roboticist In The House?
---------------------------------------------
Volume 15 Issue 01

*Ken Birman* - __31 August 1993__

A robot sent to disarm an explosive device spun out of control on a Wednesday night.
>
> 'It was just spinning round going wild!' said Edward Ellestad, a member of the Police Department's bomb squad.

The police robot went out of control outside the C&B Cafe, where a pipe bomb had been detected.

[Is there a roboticist in the house? 31 August 1993, Volume 15 Issue 01](https://catless.ncl.ac.uk/Risks/15.01.html#subj1)

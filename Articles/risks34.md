# The End of Encrypted Web and VPN Connections
---------------------------------------------
Volume 29 Issue 04

*Lauren Weinstein*  - __15 October 2015__

### Can the NSA break trillions of encrypted Web and VPN connections?
Despite years of pushing from privacy advocates, our current defense against surveillance from state-sponsored spies is under threat. A serious flaw in the current implementation of the Diffie-Hellman cryptographic key exchange may have already been exploited by these agencies, leaving our communications exposed to surveillance.

The flaw mentioned above is best summarized in this quote:

>"Since a handful of primes are so widely reused, the payoff, in terms of connections they could decrypt, would be enormous."

It takes about a year to crack a single 1024-bit prime (along with several million dollars), but because only a few of these keys are currently being used, breaking one would give these agencies access to a multitude of VPNs and SSH servers.

>"Breaking a single, common 1024-bit prime would allow NSA to passively decrypt connections to two-thirds of VPNs and a quarter of all SSH servers globally. Breaking a second 1024-bit prime would allow passive eavesdropping on connections to nearly 20% of the top million HTTPS websites."

With its $11 billion-per-year budget dedicated to 'groundbreaking cryptanalytic capabilities', the NSA may someday make that one-time investment in massive computation to eavesdrop on trillions of encrypted connections. If it has not already done so.

[How The NSA Can Break Trillions of Encrypted Web and VPN Connections; 15 October 2015, Volume 29 Issue 04](https://catless.ncl.ac.uk/Risks/29.04.html#subj5)

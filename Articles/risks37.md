# Why the Attack on Tor Matters
---------------------------------------------
Volume 29 Issue 09

*Henry Baker* - __12 November 2015__

### On Ethics in the Research Community
Recently, the Tor project posted a blog entry, accusing Carnegie Mellon University (CMU) of accepting $1 million to conduct an attack on the Tor network. A spokesperson for CMU did not deny the allegations but demanded better evidence, stating that he was not aware of any payment.

While not necessarily a bad thing, as the people identified by the de-anonymization research were accused of possessing child pornography, attention has to be paid to one small detail: __the defendants may not have been the only people affected during the attack__.

>"If the details of the attack are as we understand them, a group of academic researchers deliberately took control of a significant portion of the Tor network. Without oversight from the University research board, they exploited a vulnerability in the Tor protocol to conduct a traffic confirmation attack, which allowed them to identify Tor client IP addresses and hidden services. They ran this attack for *five* months and potentially de-anonymized thousands of users."

While most computer science researchers are believed to be fundamentally ethical people, as a community, we have a blind spot when it comes to the ethical issues in the field. There is a view that computer security research cannot really hurt people, so there is no need for ethical oversight. But this is dead wrong, and if computer science is to be taken seriously as a mature field, something has to be done about it.

There must be an understanding that active attacks affect vulnerable users (especially those in countries with restricted Internet access, or in unstable regions) and can be dangerous. It should never be conducted without rigorous oversight -- if it should even be conducted at all.

[Tor Users Matter; 12 November 2015, Volume 29 Issue 09](https://catless.ncl.ac.uk/Risks/29.09.html#subj20)

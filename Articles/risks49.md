# Hacking The Hotel TV -- And More!
---------------------------------------------
Volume 23 Issue 95

*Florian Liekweg* - __31 July 2005__

### Free Hotel Facilities!
Adam Laurie, tech director of London security and networking firm "The Bunker", apparently got bored on a recent trip; so he hacked the hotel's TV system, which allows customers to not just watch 'normal' TV programs, but also provides access to not-safe-for-work flicks and the Internet for a fee.

A laptop running Linux, its lrDA port and a USB TV tuner were all the tools needed to trick the TV into doing more than it was supposed to do, including gaining access to paid-for services, snooping on the TV watching habits of the other guests, their Internet browsing habits and, more importantly, their e-mails. The "coding" system used for infrared-based access control to the hotel minibars were just as vulnerable to hacking as their TV systems were.

[Hacking the Hotel TV -- and More; 31 July 2005, Volume 23 Issue 95](https://catless.ncl.ac.uk/Risks/23.95.html#subj11)

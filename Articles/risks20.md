# ATMs On The Offensive
---------------------------------------------
Volume 25 Issue 77

*Jeremy Epstein* - __26 August 2009__

### Should ATMs be capable of defending themselves?
A bank in South Africa endeavours to find the answer.

ATMs are incredibly vulnerable machines. It's one of the few places people show their PIN numbers and they store large volumes of money. In the last year over 500 ATMs were blown open in South Africa and one bank has had enough.

They have decided to fit some of their machines with pepper spray which is ejected when software informs the spraying machine that the ATM is being tampered with. Ideally this will stun the culprit until police arrive. A sound plan in theory however three technicians have already been accidently sprayed when preforming routine maintenance on a machine and required treatment from paramedics.

This raises questions about using such a potent method to protect ATMs. Equally, would it be possible for criminals to hack into the software controlling the pepper spray and use it on ATM customers in order to steal their belongings? All I know is, I won't be looking at ATMs the same way again.

[Pepper-spray ATMs; 26 August 2009, Volume 25 Issue 77](https://catless.ncl.ac.uk/Risks/25.77.html#subj6)

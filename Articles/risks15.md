# Man Trapped For Hours By Payphone
---------------------------------------------
Volume 23 Issue 05

*Mark Brader* - __18 November 2003__

A man in East St. Louis got his middle finger stuck in a payphone's coin return slot. Eventually, the phone was removed and taken, with the victim, to a hospital emergency room where doctors managed to pry them apart.

[Man trapped for hours by payphone; 18 November 2003, Volume 23 Issue 05](https://catless.ncl.ac.uk/Risks/23.05.html#subj13)

# The Truth About Flying Saucer Stories
---------------------------------------------
Volume 14 Issue 44

*Christopher Maeda* - __28 March 1993__

### Cheshire Police's ingenious solution to unwanted listeners
The police use radios for transmission of messages between squad cars and the police headquarters. These messages contain sensitive information regarding crimes and other wrong doings that are not suitable for the general public to be aware of at that time. Having said that, scanning devices capable of listening to these messages are easily available and perfectly legal to purchase but using them to do so is illegal.

When officers in Warrington, Cheshire were made aware of people listening in on their messages they decided to lay a trap. They sent a message detailing a crashed spaceship in a field with specific directions. Anyone listening in arrived at the field not to find E.T but police officers. A clear warning then, that eavesdropping has it's consequences.

[If they mention flying saucers, they're out to get you; 28 March 1993, Volume 14 Issue 44](https://catless.ncl.ac.uk/Risks/14.44.html#subj3)

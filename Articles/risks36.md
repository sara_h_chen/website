# Can ISIS Hackers Target Critical Infrastructure?
---------------------------------------------
Volume 29 Issue 07

*Alister Macintyre* - __31 October 2015__

### Mixed Views on the ISIS Threat
While experts cannot seem to agree on just how serious a threat ISIS is, in terms of cyberwarfare, there is a general consensus that they currently lack the technical capability needed to take down critical infrastructure.

>"Hackers can't take down the entire, or even a widespread portion of the U.S. electric grid," Jonathan Pollet, an ethical hacker and a founder of Red Tiger Security, wrote for Business Insider. "From a logistical standpoint, this would be far too difficult to realistically pull off -- and it's not what we should be devoting our attention to."

But what we should be devoting our attention to is the widespread use of social media to spread propaganda and for recruiting. Cutting them off from the web is an unfeasible solution, and removing content from the Internet is costly. Increased surveillance, however, is not the solution, especially in wake of the Edward Snowden leaks.

Perhaps the best course of action in the meantime would be to beef up security for critical infrastructure, such as nuclear power plants, as a precaution; especially since there is a growing concern that highly capable hacking software could be bought on the black market, leaving energy companies, fuel refineries and water-pumping stations vulnerable to advanced cyberattacks.

[ISIS Hackers Can Target Critical Infrastructure? 31 October 2015, Volume 29 Issue 07](https://catless.ncl.ac.uk/Risks/29.07.html#subj13)

# Friday the 13th... Again
---------------------------------------------
Volume 8 Issue 11

**Peter Neumann** - __18 January 1989__

There were various reports of Friday the 13th virus deletions in Britain attacking MS-DOS systems.

> "The virus has been frisky and hundreds of people, including a large firm with over 400 computers, have telephoned with their problems"

The virus reportedly bore similarities to the Friday the 13th Israeli virus the previous Friday the 13th.

[Friday the 13th... Again; 18 January 1989, Volume 8 Issue 11](https://catless.ncl.ac.uk/Risks/8.11.html#subj5)

# Solar Parking Meters vs British Weather
---------------------------------------------
Volume 21 Issue 65

*David Mediavilla Ezquibela* - __6 September 2001__

### Is a system run entirely on solar power viable in a climate such as Britain's?
Nottingham Council seemed to believe so when they installed 215 new parking meters that were powered solely by solar energy. Systems like this had worked well in many southern European countries but Nottingham had a few problems.

As anybody who has lived in Britain for a few years will attest, the sun is a rarity for the vast majority of the year. It's no wonder then, that these meters were not working as expected. Many of them failed and allowed free parking for large periods of the day and some simply wouldn't work as they were placed in the shade of nearby trees. The meters have since been adjusted to allow for the lower levels of solar energy available in urban Britain. Perhaps next time, investing in some sort of 'rain-powered' system would be more suitable?

[Solar parking meters are a bad idea in wet Britain; 6 September 2001, Volume 21 Issue 65](https://catless.ncl.ac.uk/Risks/21.65.html#subj12)

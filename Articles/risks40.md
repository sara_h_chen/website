# The Price of Materialism
---------------------------------------------
Volume 23 Issue 68

*NewsScan* - __21 January 2005__

### E-Waste Is A Growing Problem
Our materialism and penchant for constant upgrades, from new cell phones to sleeker laptops -- is wreaking havoc on the environment, with technology products now accounting for as much as 40% of the lead in U.S. landfills. E-waste has become one of the fastest-growing sectors of the U.S. solid waste stream.

>"The International Association of Electronics Recyclers estimates that Americans dispose of 2 million tons of electronic products a year -- including 50 million computers and 130 million cell phones."

China, which has been collecting these used devices for years, is becoming overwhelmed by the sheer volume being generated. Although some high-tech companies like Hewlett Packard and Dell have begun tackling the problem, by outsourcing their e-waste handling to environmentally sensitive recyclers like RetroBox, too little is being done to cope with this problem that has already reached crisis proportions.

>"Meanwhile, the U.S. is the only developed country not to have ratified the 1992 Basel Convention, the international treaty that controls the export of hazardous waste."

[E-waste Is Piling Up; 21 January 2005, Volume 23 Issue 68](https://catless.ncl.ac.uk/Risks/23.68.html#subj3)

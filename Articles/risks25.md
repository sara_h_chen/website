# Formula 1's Control-System Failures
---------------------------------------------
Volume 21 Issue 48

*Stellios Keskinidis* - __8 June 2001__

### As the cars become more technologically advanced, will the number of failures increase?
This was certainly the case when launch and traction control systems were introduced to the sport. Although some adapting to the systems would be required, the number of stalls at the start of races was abnormal. Most of the stalling was due to software glitches rather than driver error which raises questions about how safe the software was to use. Unfortunately, in a sport so competitive, a team can't afford to be left behind in a technological sense.

These time restraints probably lead to the software not being tested correctly which is dangerous to say the least. A stalled car on the grid is hard to avoid on a wide track but multiple cars stalling and on a smaller grid like Monaco would lead to disaster. Formula 1 has become much safer than it used to be but it should not be assumed completely safe, there is certainly danger in a car travelling so fast especially if the software running it hasn't been properly tested.

Formula 1 has always been at the cutting edge of automotive technology and although this generally leads to great advancements, a new idea is not always a good or safe one.

[Formula 1's string of control-system failures; 8 June 2001, Volume 21 Issue 48](https://catless.ncl.ac.uk/Risks/21.48.html#subj4)

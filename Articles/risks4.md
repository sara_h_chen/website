# A Slight Change in the Flight Plan
---------------------------------------------
Volume 15 Issue 01

*Dave Horsfall* - __16 March 1995__

On the final approach of an Airbus A340 to Gatwick, both pilots screens went blank to be replaced by a message saying, "Please wait...".  

The pilots tried to turn the plane left but it turned right instead, followed by a 9 degree plummet instead of the 3 degree approach the pilots attempted. Fortunately they managed to gain manual control and landed safely.

[A slight change in the flight plan; 16 March 1995, Volume 15 Issue 01](https://catless.ncl.ac.uk/Risks/15.01.html#subj1)

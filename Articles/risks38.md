# Anonymous vs. ISIS
---------------------------------------------
Volume 29 Issue 11

*Peter G. Neumann (Charlie Firestone)* - __19 November 2015__

### Netpolitik After the Paris Attacks
The filmed declaration of Anonymous to take down ISIS is the epitome of netpolitik -- a concept which postulates that "realpolitik" and "international liberalism" no longer sufficed as diplomatic models to resolve world problems.

For the first time, a non-governmental network of anonymous hackers will fight against an international network of violent terrorists on the cyber warfield. The terrorist network that has been so savvy with new media now faces a network of accomplished hackers.

>"This, then, is a prime example of netpolitik, the engagement of networks to counter networks - though in this case it goes beyond diplomacy. It is an early episode in guerilla cyber warfare."

The fight against ISIS will require all kinds of efforts; in the air, on the ground, and now, over the ether. Which method will prove to be most efficient in taking down terrorist groups, as they become increasingly sophisticated?

>"Today, nations should not have to rely on the white masks of digital vigilantes. Rather, they need cyber-rangers with the capabilities -- talent, resources, resolve -- to defeat their enemies, who more likely than not, will be networks, not nations."

[Anonymous vs. ISIS: Netpolitik After the Paris Attacks; 19 November 2015, Volume 29 Issue 11](https://catless.ncl.ac.uk/Risks/29.11.html#subj3)

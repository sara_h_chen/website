# IBM's Infamous Johnson Bug
---------------------------------------------
Volume 17 Issue 39

*Jason Fleischer* - __16 October 1995__

### What was the Johnson Bug?
IBM is one of the worlds largest tech companies, but their rise to power has not been without mishap. A good example of this is the Johnson Bug.

The bug resided within the operating system of an IBM 370 but was discovered in a rather unusual way. Dr Gerry Johnson was demonstrating a new piece of networking software to link PC's to the IBM global network. To test the limits of the software, Dr Johnson asked for a file to be transferred from a 370 a few miles away to his PC. However, the file was larger than the hard drive of the machine, his argument being if the software was written correctly it would error out. Unfortunately this was not the case and both the 370 and the PC crashed.

Dr Johnson later discovered that as a result of his experiment, not only had the 370 and the PC gone down, so had the entirety of IBM's global network. The culprit was a bit of code in the 370's OS that had not been executed in it's 15 years of service.

[The Johnson Bug - IBM; 16 October 1995, Volume 17 Issue 39](https://catless.ncl.ac.uk/Risks/17.39.html#subj3)

# Insufficient Testing and Amusement Rides
---------------------------------------------
Volume 11 Issue 31

*Peter da Silva* - __16 March 1991__

### With something so dangerous, how can we be sure amusement rides are tested to a satisfactory level?
Thrill seekers and adrenaline junkies alike search for the best and most thrilling rides and rollercoasters. Nothing matches the sense of danger from being thrown about and held in by nothing but a bar or harness but with the forces this exerts and the potential for wear and tear, how can we be sure we're safe?

Theme parks undertake regular testing of their rides before opening them to the public but as we know, this sometimes isn't enough. One of the less gruesome examples in the Condor at Astroworld, Texas. Before the grand opening reporters were given the opportunity to ride and a modification was made to the rides software so it would stop at the top and pictures could be taken. Although the stop worked, the ride wouldn't start again and need to be coaxed down to release the reporters. This problem should have easily been identified by testing and all though the event was nothing but annoying, errors like this in different situations, such as a fast rollercoaster, could be fatal.

[Untested mods to amusement rides; 16 March 1991, Volume 11 Issue 31](https://catless.ncl.ac.uk/Risks/11.31.html#subj1)

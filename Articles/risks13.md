# Name This Book -- For A Box of Cookies!
---------------------------------------------
Volume 8 Issue 06

*Cliff Stoll* - __10 January 1989__

I am writing a book and I need a title.  

It is about computer risks: counter-espionage, networks, computer security and a hacker/cracker that broke into military computers. It's a true story about how we caught a spy secretly prowling through the Milnet.

[Name this book; 10 January 1989, Issue 06](https://catless.ncl.ac.uk/Risks/8.6.html#subj8)

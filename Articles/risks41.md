# Eavesdropping Trees
---------------------------------------------
Volume 23 Issue 72

*Dan Jacobson* - __16 February 2005__

### Remains of Trees Used to Conceal GSM Antennas
>"The product used in Palm antennas is formed by the tree itself and the fronds."

GSM base stations are being camouflaged in specially preserved palm trees, with antennas that were designed to look just like palm fronds. These trees have internal steel-bar reinforcements installed for structural rigidity, and all cable works are done within the trunk.

>"They won't be doing much by themselves after they've been eviscerated. But they could serve other purposes as well."

Perhaps the biggest risk here is in assuming that these plants are not doing anything special. It would also seem that the rumors were true, about plants being able to hear you!

[Trees with Concealed GSM Antennas; 16 February 2005, Volume 23 Issue 72](https://catless.ncl.ac.uk/Risks/23.72.html#subj5)

# 'Wrong Country' Sat-Nav Blunder
---------------------------------------------
Volume 24 Issue 93

*Richard Weir* - __22 December 2007__

Shoppers on a Christmas trip to France were taken to the wrong country after a Sat-Nav diverted their coach seven hours off course, to Lille in Belgium.
>
> "Unfortunately the driver from the coach company we commissioned made a blunder on his satellite navigation."
>

['Wrong Country' sat-nav blunder; 22 December 2007, Volume 24 Issue 93](https://catless.ncl.ac.uk/Risks/24.93.html#subj5)

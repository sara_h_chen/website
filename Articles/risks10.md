# Three Gremlins On The Loose: Nukes, Sharks and Enlightened Rockets
---------------------------------------------
Volume 5 Issue 02

*Dave Platt* - __11 June 1987__

Sharks have appeared to have taken a fancy for the new 1" thick underwater cables being strung across the Atlantic; they've bitten through the cable at least four times.  

More than one of every three nuclear weapons in the US doesn't work properly.  

A lightening strike on a pad at NASA's launch facility ignited three small rockets and sent two of them hurtling along their planned trajectories.

[Three gremlins on the loose: nukes, sharks and enlightened rockets; 11 June 1987, Volume 5 Issue 02](https://catless.ncl.ac.uk/Risks/5.2.html#subj1)

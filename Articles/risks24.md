# Nuclear Warheads Armed by Malfunction
---------------------------------------------
Volume 19 Issue 14

*Matt Welsh* - __13 May 1997__

### Computer malfunctions and sets Russian nuclear weapons to 'combat mode'
Computer malfunctions are far from uncommon and are generally easily fixed, turning it off and on again is a classic trick. But when the malfunction concerned involves weapons of mass destruction, more than a few eyebrows are raised.

Computer malfunctions are exceedingly worrying on systems of this kind, but is it unfair to expect these kind of events to never occur?

Luckily, the malfunction only increases the risk of accidental launch and additional codes would still be required which were still controlled by Moscow. Still, a worrying prospect that even the most sophisticate weapons could malfunction and cause untold destruction. Sleep well.

[Russian nuclear warheads armed by computer malfunction; 13 May 1997, Volume 19 Issue 14](https://catless.ncl.ac.uk/Risks/19.14.html#subj1)

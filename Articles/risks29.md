# The BBC's Attack on the Library of Congress
---------------------------------------------
Volume 5 Issue 5

*Mark Brader* - __25 June 1987__

### Although accidental, this attack wreaked havoc on the libraries main database
As the largest library in the world, the Library of Congress receives considerable attention. Their main database was of particular interest to reporters for the amount of information it had to store. The machines themselves were IBM and Amdahl mainframes with STC tape drives.

One of the interested parties was the BBC who were given access to much of the library for the filming of a documentary. However, whenever camera crews entered the computer room that stored the database machines, all major systems would crash. Specifically, the problem was all the tape drives would stop, rewind and unload at the same time. The OS could not handle such simultaneous activity and so crashed. However, the cause of this was unclear.

Of course a culprit was found, not film but photography. Most work done by BBC camera crews was video but the occasional picture was also taken. The flash of the camera would reflect into to photoelectric end-of-tape sensors of each drive and cause all drives to read end-of-tape. This lead to all the tapes in range of the flash to rewind and crash the system.

[BBC documentary filming causes Library of Congress computer crashes; 25 June 1987, Volume 5 Issue 5](https://catless.ncl.ac.uk/Risks/5.5.html#subj2)

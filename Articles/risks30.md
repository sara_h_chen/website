# Auto-Pilots In Cars
---------------------------------------------
Volume 9 Issue 14

*Pete Lucas* - __21 August 1989__

### With the dawn of the Google car it seems we are closer than ever to driverless cars
However, concerns and ideas about 'cars with autopilots' were around much before this.

The thoughts of cars driving themselves, and which are capable of reacting much faster than humans and communicating with other cars to create the perfect flow of traffic, may excite some people it quite rightly terrifies others. Systems may be capable of driving a car but there is always the potential for error. Glitches in software or damage/wear and tear to sensors could render the computer driver inoperable or dangerous. Furthermore, systems for navigation are rarely perfect. The maps of sat-navs occasionally show roads that no longer exist or won't be aware of a new road, all leading to danger as the computer driver is ignorant to the new danger.

There is always a story of someone following their navigation into a river, could it be possible for a computer driver to do the same? Although human drivers are far from perfect, the roads are designed for them and they do work most of the time. Maybe best to keep it simple for a little while longer.

[Automatic vehicle navigation systems; 21 August 1989, Volume 9 Issue 14](https://catless.ncl.ac.uk/Risks/9.14.html#subj3)

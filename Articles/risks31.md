# The Dangers of Automate Systems and Money
---------------------------------------------
Volume 27 Issue 37

*Amos Shapir* - __21 July 2013__

### Paypal overcharge Pennsylvanian man by $92 quadrillion
This all highlights the problems with automated systems when placed in control of money. The exact figure is equal to 2 to the power 63 multiplied by 0.01 so the bill was likely for one cent until a computer error edited the number. Had this value been less ridiculous and possibly passed from one party to another then the victim may very well have been charged the amount when in reality he owed very little.

The number was obviously immediately recognised as an error and amended, but the man was rather surprised initially. Paypal also stated they would make a donation to a charity of the man's choice to make up for the mishap.

[PayPal 'credits' US man $92 quadrillion in error; 21 July 2013, Volume 27 Issue 37](https://catless.ncl.ac.uk/Risks/27.37.html#subj4)

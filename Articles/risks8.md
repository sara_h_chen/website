# Army to Use Macs to Prevent Hacking
---------------------------------------------
Volume 24 Issue 93

*Peter Houppermans* - __21 December 2007__

The military are quietly working to integrate Macintosh computers into systems to make them harder to hack. This is due to fewer attacks being designed to infiltrate Mac computers.

Adding more Macs to the computer makes it tougher to de-stabilise a group of military computers with a single attack.

[Army to use Macs to prevent hacking; 21 December 2007, Volume 24 Issue 93](https://catless.ncl.ac.uk/Risks/24.93.html#subj4)

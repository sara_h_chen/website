# Dangers of Trading In Your Old Phone
---------------------------------------------
Volume 20 Issue 15

*Craig DeForest* - __6 January 1999__

### A lot of private information is stored on a mobile phone, which we wouldn't want others having access to.
Trading in and buying old phones is common practice and is a very good way of saving money, but it is not without it's inherent risks.

A case in Colorado describes how a man bought a phone supposedly new but had fingerprints and a scratch on the protective cover. This was of no concern as the phone was cheap enough. What was concerning was the phone directory still had 6 numbers programmed into it.

Data protection is vastly important in a world so interconnected and a simple mistake such as not wiping your old phone before selling it could leave you vulnerable to attack. Luckily for the previous owner of the phone, Craig was not interested in selling the information and simply deleted the contacts.

[Cell-phone surprise; 6 January 1999, Volume 20 Issue 15](https://catless.ncl.ac.uk/Risks/20.15.html#subj3)

# China Recruiting Hackers to Exploit Vulnerabilities in U.S. Security Systems  
-----------------------------------------------------------
Volume 29 Issue 06

*Prashanth Mundkur* - __28 October 2015__

### Shortage of Hackers to Exploit Vulnerabilities in U.S. Security Systems
The role of technology in modern politics is often overlooked. But, hidden from mainstream media, the Chinese government has devoted countless resources to recruit hackers to exploit vulnerabilities in U.S. security systems.

However, despite their headhunting efforts, the Chinese are struggling to keep up with the sheer number of vulnerabilities they are finding.

>"With new weaknesses in U.S. networks popping up every day, we simply don't have the manpower to effectively exploit every single loophole in their security protocols," said security minister Liu Xiang.

The minister then went on to confirm that the thousands of Chinese computer experts employed to expose flaws in American data systems are just no match for the United States' increasingly ineffective digital safeguards.

>"We can't keep track of all of the glaring deficiencies in their firewall protections, let alone hire and train enough hackers to attack each one. And now, they're failing to address them at a rate that shows no sign of slowing down anytime soon. *The gaps in the State Department security systems alone take up almost half my workforce*."

It is hard to tell which is more concerning -- the fact that the safety of millions of Americans are at risk, and that they are completely oblivious to it, or the fact that the inadequate labor pool has forced China to outsource some of its hacker work to Russia.

[China Unable to Recruit Hackers Fast Enough to Keep Up With Vulnerabilities in U.S. Security Systems; 28 October 2015, Volume 29 Issue 06](https://catless.ncl.ac.uk/Risks/29.06.html#subj1)

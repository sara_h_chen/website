# Profiting From Misdialed Numbers
---------------------------------------------
Volume 16 Issue 96

*Matt Weatherford* - __22 March 1995__

A company were offering an 800-operator service for discounted Collect calls.  
A competitor realised than many people were accidentally calling the wrong number and so chartered this number for their own, raking in the stray dialers, who were charged for their calls.

[Profiting from misdialed numbers; 22 March 1995, Volume 16 Issue 96](https://catless.ncl.ac.uk/Risks/16.96.html#subj6)

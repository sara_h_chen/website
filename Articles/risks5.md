# Microsoft Puts A Price On the Heads of Virus Writers
---------------------------------------------
Volume 23 Issue 01

*NewsScan* - __6 November 2003__

Microsoft has created a $5-million Anti-Virus Reward Program and is offering $250,000 bounties for information leading to the arrest and conviction on the people behind last summer's Blaster worm and Sobig virus.  
Microsoft Senior Security Strategist Phillip Reitinger says:  
> "What we hope to accomplish is to give people an incentive to do the right thing."

[Microsoft puts a price on the heads of virus writers; 6 November 2003, Volume 23 Issue 01](https://catless.ncl.ac.uk/Risks/23.01.html#subj8)

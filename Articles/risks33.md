# Haunted Channel Tunnel
---------------------------------------------
Volume 17 Issue 3

*Lord Wodehouse* - __2 April 1995__

### Could the channel tunnel really be infested with ghost trains?
The channel tunnel is over 31 miles long and took 6 years to complete and, unsurprisingly, there are problems with operating a train line underwater. Sea water is clearly the largest danger to the trains and it's passengers and although nobody has drowned, the water is causing problems in other ways.

Drivers were reporting receiving 3 red flashing lights on the console which is the signal for an emergency stop and had to wait until controllers checked the position of every train in the tunnel to determine if it's safe to continue. This happened multiple times a week with no actual need for the emergency stop. The reason for the lights fell on the sea water. When a train went through the tunnel it raised a mist of salt water which short-circuited a connection between rails, like a train would. This 'ghost train' was in the path of a real train which had to stop until the ghost had passed.

Sea water was always going to be the major difficulty for a project like the channel tunnel and is likely to continue presenting problems in future, even if that's just the corrosive nature of salt water on metal.

[Chunnel has ghost trains; 2 April 1995, Volume 17 Issue 3](https://catless.ncl.ac.uk/Risks/17.03.html#subj1)

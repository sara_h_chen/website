# Canadian Nuclear Plant Leaks Into Lake Ontario
---------------------------------------------
Volume 26 Issue 38

*Geoff Goodfellow* - __19 March 2011__

### North America Faces Radiation Danger
Completely ignored by mainstream media, which has been too busy focusing on the Japanese radiation leak as well as the toxic plume of radioactive particles (possibly containing uranium and plutonium) heading for the United States, Canada's Ontario Power Generation has quietly released radioactive water into Lake Ontario.

This follows a leak in the Pickering A nuclear generating station, as a result of what appears to be a pump seal failure, causing tens of thousands of litres of radioactive water to end up in Lake Ontario.

>"No doubt this is an attempt to hush concern over __another radioactive accident__ amid anxiety over the catastrophe in Japan."

Although concerning for a variety of reasons, this leak is especially alarming when taking into account the fact that: not only is Lake Ontario one of the five Great Lakes of North America, it is also the main source of drinking water for millions of people.

[Canadian Nuclear Plant Leaks Radioactive Water Into Lake Ontario; 19 March 2011, Volume 26 Issue 38](https://catless.ncl.ac.uk/Risks/26.38.html#subj2)

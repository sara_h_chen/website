# Armenia Literally Cut Off From the Internet
---------------------------------------------
Volume 26 Issue 41

*Lauren Weinstein (Adrian Chen)* - __6 April 2011__

### 75-Year-Old Guilty
A 75-year-old lady from the Republic of Georgia was scavenging for scrap metal around the village of Ksanai on March 28 when she came across a fibreoptic cable linking Armenia to Georgia. She decided to cut it up to sell the copper.

Unfortunately, that sole cable provided the majority of Armenia's internet service, and the poor Armenians were disconnected from the Internet for "hours" that evening. As a result, this lady now faces up to three years in prison.

Perhaps it should come as a surprise that such a vital component of Armenia's infrastructure was left in a state so exposed that it could be found and tampered with so easily. Maybe Armenia should have given more thought to the dangers in relying on a single, fragile cable to connect the entire country to the Internet.

[75-Year-Old Woman (Literally) Cuts Armenia Off the Internet; 6 April 2011, Volume 26 Issue 41](https://catless.ncl.ac.uk/Risks/26.41.html#subj10)

# Your iPhone Tracks You
---------------------------------------------
Volume 26 Issue 44

*Matthew Kruk (Nick Bilton)* - __21 April 2011__

### Devices Found Recording Locations in A Hidden File
Apple was questioned about the security of its iPhone and iPad after a report revealed that the devices regularly recorded their locations in a hidden file. The report came from a technology conference in San Francisco, where two programmers presented their research, showing that the iPhone and 3G versions of the iPad had begun logging users' locations since a year ago when Apple updated its mobile operating system.

When the device is synced to a computer, the hidden file containing location data that is gleaned from nearby cellphone towers and Wi-Fi networks is copied over to the hard drive. The risk here is that the file is usually left unencrypted; although users are given the option to encrypt this file, only very few actually do.

This raises major concerns on the impact that such a move from Apple would have on users' privacy and security.

>"The secretive collection of location data crosses the privacy line," said Marc Rotenberg, executive director of the Electronic Privacy Information Center. "Apple should know better than to track iPhone users in this way."

[Tracking File Found In iPhones; 21 April 2011, Volume 26 Issue 44](https://catless.ncl.ac.uk/Risks/26.44.html#subj11)

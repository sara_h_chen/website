# Train Crash Blamed On 'Serious Design Flaws'
---------------------------------------------
Volume 26 Issue 51

*Jim Reisert* - __30 July 2011__

### Can we solely blame software and equipment for failures?
Transportation continues to get faster and railways are under constant pressure to be upgraded and maintained to accommodate these high speed services. This does not always occur as a crash in Zhejiang Province, China proved.

During the incident in question, a high-speed train rammed into a stationary train, leaving 40 dead and 191 injured. The blame was placed on signalling equipment with 'serious design flaws'. A lightning strike had damaged the equipment and permitted the moving train into the path of the stationary one.

Although this may be a satisfactory explanation for some, there are questions that should be asked:
  - Why was this seriously flawed equipment in use?'
  - Who was responsible for the flaw?'
  - Are these the only problems or are there more hidden gremlins?

[China train crash explanation raises more public doubts; 30 July 2011, Volume 26 Issue 51](https://catless.ncl.ac.uk/Risks/26.51.html#subj1)

# Car Computer Systems At Risk to Viruses
---------------------------------------------
Volume 23 Issue 96

*Peter G Neumann* - __2 August 2005__

Car industry officials and analysts say hackers are growing interest in writing viruses for wireless devices puts auto computer systems at risk of infection. By adjusting onboard computers in cars allowing consumers to transfer information with MP3 players and mobile phones, they are making vehicles vulnerable to mobile viruses that jump between devices via Bluetooth.

[Car computer systems at risk to viruses; 2 August 2005, Volume 23 Issue 96](https://catless.ncl.ac.uk/Risks/23.96.html#subj3)

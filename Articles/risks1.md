# Risks of Robots
---------------------------------------------
Volume 5 Issue 84

*Eric Haines* - __30 December 1987__

The US Department of Agriculture has encountered some difficulties in their project to develop robot fruit pickers. The monochrome scanners the robots were designed with have made them unable to distinguish between an orange and a small cloud and so many have been left literally reaching for the clouds.  
Hopes are to develop the robot pickers using colour in the near future.

[Risks of Robots; 30 December 1987, Volume 5 Issue 84](https://catless.ncl.ac.uk/Risks/5.84.html#subj1)

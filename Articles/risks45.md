# The Latest Surveillance Weapon: Facial Recognition Software
---------------------------------------------
Volume 26 Issue 51

*Steven J. Klein* - __1 August 2011__

### Facebook and Facial Recognition Are A Deadly Combo
Using a combination of Facebook and pittpatt facial recognition software, about one third of all randomly photographed people on the campus of Carnegie Mellon University could later be identified by name. About 27% of those identified had enough information on their Facebook profiles to allow the first five digits of their Social Security numbers to be correctly predicted.

Beginning with just a photo of their faces, individuals could be identified, even on sites that use pseudonyms to protect the privacy of their members.

A smartphone application was also built to demonstrate the ability of making the same sensitive inferences in real-time. The application uses both offline and online data to overlay personal and private information over the target's face on the device's screen.

>"As if the above isn't sufficiently disturbing on its own, Google just purchased pittpatt, the developer of the facial recognition used for this experiment."

[FaceBook + Facial Recognition Software = Increase Privacy Risks; 1 August 2011, Volume 26 Issue 51](https://catless.ncl.ac.uk/Risks/26.51.html#subj5)

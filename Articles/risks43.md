# British Spies Replace Terrorists' Online Bomb Instructions
---------------------------------------------
Volume 26 Issue 48

*Monty Solomon (Paisley Dodd)* - __12 June 2011__

### Make Cupcakes, Not War
Intelligence agents managed to hack into the extremist Inspire magazine, replacing its bomb-making instructions with a recipe for cupcakes. This was the first time the agents sabotaged the English-language magazine linked to U.S.-born Yemeni cleric Anwar al-Awlaki, an extremist affiliated with al-Qaeda and accused in several recent terror plots.

The quarterly online magazine, which is distributed as a PDF file, had featured an original page titled "Make a Bomb in the Kitchen of Your Mom" in one of its editions last year. In the cyber warfare operation dubbed 'Operation Cupcake', which was launched by MI6 and GCHQ in an attempt to disrupt efforts by al-Qaeda in the Arabian Peninsular to recruit "lone-wolf" terrorists, code was inserted into the original magazine that loaded up a web page of recipes for "__The Best Cupcakes in America__" published by the Ellen DeGeneres show.

[British Spies Replace Terrorists' Online Bomb Instructions; 12 June 2011, Volume 26 Issue 48](https://catless.ncl.ac.uk/Risks/26.48.html#subj10)

# 7 Inmates Escape: Computer Blamed!
---------------------------------------------
Volume 5 Issue 08

*Peter G Neumann* - __7 July 1987__

On the 4th of July in Santa Fe, New Mexico, a prisoner kidnapped one guard, shot another, commandeered the control center and released six other prisoners. All 7 went through an emergency roof door, pole vaulted over a barbed wire fence and disappeared.  
It was noticed that the prison computer control system was down at the time and otherwise would have prevented the escape!

[7 Inmates Escape: Computer Blamed! 7 July 1987, Volume 5 Issue 08](https://catless.ncl.ac.uk/Risks/5.8.html#subj2)

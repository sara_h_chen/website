# Eurofighter Typhoon's Faulty Brakes
---------------------------------------------
Volume 23 Issue 2

*Peter B. Ladkin* - __12 November 2003__

### Perhaps an anchor would be more effective?
Fighter jet technology is some of the most advanced in the business and is a must have for any combat efficient fighter. More accurately, many of todays jet fighters would be completely incapable of flight without the assistance of multiple, on-board computers. Understandable then, when a computer lead to the failure of the braking system on a Typhoon there were many worried looks on the faces of engineers and pilots alike.

The incident occurred as a warning light came on during a routine landing and as the pilot deployed the braking parachute the brakes themselves refused to work. The aircraft was grounded for 3 weeks until the problem was confirmed to be attributed to a faulty microchip in the landing gear computer. All this occurred during the testing of these aircraft, which may bring some consolation. However, worst case scenario would be for this kind of event to occur in a combat situation and is it so farfetched to assume it might?

[Eurofighter Typhoon brake fault; 12 November 2003, Volume 23 Issue 2](https://catless.ncl.ac.uk/Risks/23.02.html#subj1)

# Hello Barbie
---------------------------------------------
Volume 28 Issue 97

*Alister Wm Macintyre* - __28 September 2015__

Mattel has made a new $75.00 wi-fi enable Barbie. She asks and answers questions, which will be stored on Toy Talk servers.  
There are public concerns over the security of the data and, consequently, the security of the children as data could easily be hacked.
It is also thought that Hello Barbie could download software updates and be used to promote future Mattel toys.

[Hello Barbie; 28 September 2015, Volume 28 Issue 97](https://catless.ncl.ac.uk/Risks/28.97.html#subj10)

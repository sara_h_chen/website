import sys, enchant, re, glob
from enchant import DictWithPWL
from enchant.checker import SpellChecker

def spellCheck(text):
    my_dict = DictWithPWL("en_GB", "validWords.txt")
    chkr = SpellChecker(my_dict)
    chkr.set_text(text)
    error = False
    for err in chkr:
        error = True
        print "ERROR: ", err.word
        print my_dict.suggest(err.word)
        print   
    if not error:
        print "No Errors"

def clean(text):
    temp = text.split("*")
    del temp[1]
    text = ""
    for i in temp:
        text = text+i
    temp = text.split("(https")
    del temp[1]
    text = ""
    for i in temp:
        text = text+i
    return text

filenames = glob.glob("Articles/*.md")
for file in filenames:
    with open(file, 'r') as f:
        lines = f.readlines()
        print lines[0]
        original = ""
        for i in lines:
            original = original + i
        text=clean(original)
        spellCheck(text)
        print lines[1]


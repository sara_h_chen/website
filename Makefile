SHELL := /bin/bash
spellCheck: Articles/*.md
	python spellChecker.py > spellingErrors.txt

markdown%: Articles/risks%.md
	python pythonMarkdownParser.py Articles/risks$*.md > risks$*.html

pagebuild%: Articles/risks%.md
	cp htmlHeader.txt risks$*.html
	python pythonMarkdownParser.py Articles/risks$*.md >> risks$*.html
	cat htmlFooter.txt >> risks$*.html
	mv risks$*.html Site/ArticleHTMLs

generateArticleList:
	cp Site/ArticleHTMLs/listHeader.txt Site/ArticleHTMLs/ArticleList.html
	make genPrivSurv
	make genCyberWarfare
	make genTechEnv
	make genHumanErrors
	make genTransportation
	make genTechAdvance
	make genCompErrors
	cat Site/ArticleHTMLs/listFooter.txt >> Site/ArticleHTMLs/ArticleList.html	 

genCompErrors:
	echo '</div><div class="item centered"><h2>Computer Errors</h2><p></p>' > Site/ArticleHTMLs/CompErrors/listOfLinks.txt
	grep -r "catless" Site/ArticleHTMLs/CompErrors/*.html | sed 's/\(<p>\).*\(\">\)//g' | sed 's/:/\">/' | sed 's/Site/<p><a href=\"http:\/\/community.dur.ac.uk\/sara.h.chen/g' >> Site/ArticleHTMLs/CompErrors/listOfLinks.txt
	cat Site/ArticleHTMLs/CompErrors/listOfLinks.txt >> Site/ArticleHTMLs/ArticleList.html
genPrivSurv: 
	echo '<div class="item active centered"><h2>Privacy & Surveillance</h2><p></p>' > Site/ArticleHTMLs/PrivSurv/listOfLinks.txt
	grep -r "catless" Site/ArticleHTMLs/PrivSurv/*.html | sed 's/\(<p>\).*\(\">\)//g' | sed 's/:/\">/' | sed 's/Site/<p><a href=\"http:\/\/community.dur.ac.uk\/sara.h.chen/g' >> Site/ArticleHTMLs/PrivSurv/listOfLinks.txt
	cat Site/ArticleHTMLs/PrivSurv/listOfLinks.txt >> Site/ArticleHTMLs/ArticleList.html
genCyberWarfare:
	echo '</div><div class="item centered"><h2>Cyber Warfare</h2><p></p>' > Site/ArticleHTMLs/CyberWarfare/listOfLinks.txt
	grep -r "catless" Site/ArticleHTMLs/CyberWarfare/*.html | sed 's/\(<p>\).*\(\">\)//g' | sed 's/:/\">/' | sed 's/Site/<p><a href=\"http:\/\/community.dur.ac.uk\/sara.h.chen/g' >> Site/ArticleHTMLs/CyberWarfare/listOfLinks.txt
	cat Site/ArticleHTMLs/CyberWarfare/listOfLinks.txt >> Site/ArticleHTMLs/ArticleList.html
genHumanErrors:
	echo '</div><div class="item centered"><h2>Human Errors</h2><p></p>' > Site/ArticleHTMLs/HumanErrors/listOfLinks.txt
	grep -r "catless" Site/ArticleHTMLs/HumanErrors/*.html | sed 's/\(<p>\).*\(\">\)//g' | sed 's/:/\">/' | sed 's/Site/<p><a href=\"http:\/\/community.dur.ac.uk\/sara.h.chen/g' >> Site/ArticleHTMLs/HumanErrors/listOfLinks.txt
	cat Site/ArticleHTMLs/HumanErrors/listOfLinks.txt >> Site/ArticleHTMLs/ArticleList.html
genTechAdvance:
	echo '</div><div class="item centered"><h2>Technological Advancements</h2><p></p>' > Site/ArticleHTMLs/TechAdvance/listOfLinks.txt
	grep -r "catless" Site/ArticleHTMLs/TechAdvance/*.html | sed 's/\(<p>\).*\(\">\)//g' | sed 's/:/\">/' | sed 's/Site/<p><a href=\"http:\/\/community.dur.ac.uk\/sara.h.chen/g' >> Site/ArticleHTMLs/TechAdvance/listOfLinks.txt
	cat Site/ArticleHTMLs/TechAdvance/listOfLinks.txt >> Site/ArticleHTMLs/ArticleList.html
genTechEnv:
	echo '</div><div class="item centered"><h2>Technology & the Environment</h2><p></p>' > Site/ArticleHTMLs/TechEnv/listOfLinks.txt
	grep -r "catless" Site/ArticleHTMLs/TechEnv/*.html | sed 's/\(<p>\).*\(\">\)//g' | sed 's/:/\">/' | sed 's/Site/<p><a href=\"http:\/\/community.dur.ac.uk\/sara.h.chen/g' >> Site/ArticleHTMLs/TechEnv/listOfLinks.txt
	cat Site/ArticleHTMLs/TechEnv/listOfLinks.txt >> Site/ArticleHTMLs/ArticleList.html
genTransportation:
	echo '</div><div class="item centered"><h2>Transportation</h2><p></p>' > Site/ArticleHTMLs/Transportation/listOfLinks.txt
	grep -r "catless" Site/ArticleHTMLs/Transportation/*.html | sed 's/\(<p>\).*\(\">\)//g' | sed 's/:/\">/' | sed 's/Site/<p><a href=\"http:\/\/community.dur.ac.uk\/sara.h.chen/g' >> Site/ArticleHTMLs/Transportation/listOfLinks.txt
	cat Site/ArticleHTMLs/Transportation/listOfLinks.txt >> Site/ArticleHTMLs/ArticleList.html

autoDeploy:
	cp -r "$(PWD)/portfolio" ~/public_html
	cp -r "$(PWD)/Site/." ~/public_html 
	#find ~/public_html -type f -iname "*.html" -print0 | xargs -0 sed -i 's/sara.h.chen/yourusername/g'

parseLinks: Site/ArticleHTMLs/*.html 
	python htmlExtractor.py Site/ArticleHTMLs/*.html | egrep -o 'https?://[^ ]+' > links.txt
	wget --spider -e robots=off -w 1 --input links.txt 2>&1 | egrep -i '(https?://[^ ]+|failed|broken|Not Found|error)' >>log.txt



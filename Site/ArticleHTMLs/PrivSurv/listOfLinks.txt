<div class="item active centered"><h2>Privacy & Surveillance</h2><p></p>
<p><a href="http://community.dur.ac.uk/sara.h.chen/ArticleHTMLs/PrivSurv/risks0.html">Hello Barbie; 28 September 2015, Volume 28 Issue 97</a></p>
<p><a href="http://community.dur.ac.uk/sara.h.chen/ArticleHTMLs/PrivSurv/risks32.html">If they mention flying saucers, they're out to get you; 28 March 1993, Volume 14 Issue 44</a></p>
<p><a href="http://community.dur.ac.uk/sara.h.chen/ArticleHTMLs/PrivSurv/risks34.html">How The NSA Can Break Trillions of Encrypted Web and VPN Connections; 15 October 2015, Volume 29 Issue 04</a></p>
<p><a href="http://community.dur.ac.uk/sara.h.chen/ArticleHTMLs/PrivSurv/risks37.html">Tor Users Matter; 12 November 2015, Volume 29 Issue 09</a></p>
<p><a href="http://community.dur.ac.uk/sara.h.chen/ArticleHTMLs/PrivSurv/risks39.html">RFID Tagging Elementary School Children; 11 February 2005, Volume 23 Issue 71</a></p>
<p><a href="http://community.dur.ac.uk/sara.h.chen/ArticleHTMLs/PrivSurv/risks41.html">Trees with Concealed GSM Antennas; 16 February 2005, Volume 23 Issue 72</a></p>
<p><a href="http://community.dur.ac.uk/sara.h.chen/ArticleHTMLs/PrivSurv/risks45.html">FaceBook + Facial Recognition Software = Increase Privacy Risks; 1 August 2011, Volume 26 Issue 51</a></p>
<p><a href="http://community.dur.ac.uk/sara.h.chen/ArticleHTMLs/PrivSurv/risks46.html">Tracking File Found In iPhones; 21 April 2011, Volume 26 Issue 44</a></p>
<p><a href="http://community.dur.ac.uk/sara.h.chen/ArticleHTMLs/PrivSurv/risks49.html">Hacking the Hotel TV -- and More; 31 July 2005, Volume 23 Issue 95</a></p>
